#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi
import os

gi.require_version("Gtk", "3.0")
gi.require_version("GtkSource", "3.0")

from gi.repository import Gtk, Gdk, GtkSource

from libs.graph_generator import TreeImage

from lexical import lexer_graphic as LexerG
from syntax import syntax_graphic as SyntaxG
from semantic import semantic_graphic as SemanticG
from libs.errors import CompilerError, LexicalError
from interface.windows import HelpDialog, Interpleter

HORIZONTAL = 0
VERTICAL   = 1
SUCCESS    = True
ERROR      = False

class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Compilador")
        self.set_default_size(1200, 720)
        self.set_hide_titlebar_when_maximized(True)
        self.set_icon_from_file("interface/images/code-flat.png")

        self.list_config_icons   = {}
        self.list_config_menu    = {}  
        self.list_config_buttons = {}   

        self.buffer_output = None
        self.buffer_code   = None
        self.buffer_mepa   = None
        self.file_path     = None
        self.mepa_file     = None

        self.widget_images       = {}
        self.widget_buttons      = {}
        self.widget_menu         = None
        self.widget_switch_graph = None
        self.widget_switch_edit  = None
        self.widget_pager        = None
        self.widget_editor       = None
        self.label_filename      = None

        # Widget that contains layout program
        self.layout = None

        self.create_configs()
        self.create_widgets_images()
        self.create_widgets_buttons()
        self.create_widget_menu()
        self.create_layout()
        
        self.add(self.layout)

        self.connect("destroy", Gtk.main_quit)
        self.show_all()
        self.style()

    def style(self):
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path('interface/app.css')
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def create_configs(self):
        self.list_config_icons = {
          "icon_clear"      : "gtk-clear",
          "icon_help"       : "gtk-help",
          "icon_lexer"      : "gtk-spell-check",
          "icon_refresh"    : "gtk-refresh",
          "icon_semantic"   : "gtk-sort-ascending",
          "icon_syntax"     : "gtk-indent",
          "icon_open"       : "gtk-file",
          "icon_generator"  : "gtk-convert"
        }

        self.list_config_menu = {
            "Abrir arquivo": self.on_file_chooser,
            "Salvar arquivo": self.on_file_save,
            # "Abrir configurações": self.nothing
        }

        self.list_config_buttons = {
            "btn_help": {
                "label": "Ajuda",
                "function": self.on_help_clicked,
                "icon": "icon_help"
            },
            "btn_lexer":{
                "label": "Lexico",
                "function": self.on_lexer_clicked,
                "icon": "icon_lexer"
            },
            "btn_syntax": {
                "label": "Sintatico",
                "function": self.on_syntax_clicked,
                "icon": "icon_syntax"
            },
            "btn_semantic":{
                "label": "Semantico",
                "function": self.on_semantic_clicked,
                "icon": "icon_semantic"
            },
            "btn_generator":{
                "label": "Gerador de codigo",
                "function": self.on_generator_clicked,
                "icon": "icon_generator"
            },
            "btn_clear":{
                "label": "Limpar Saida",
                "function": self.on_clear_clicked,
                "icon": "icon_clear"
            }

        }

    def create_widgets_images(self):
        for name, icon in self.list_config_icons.items():
            image = Gtk.Image.new_from_stock(icon, Gtk.IconSize.BUTTON)
            self.widget_images[name] = image

    def create_widgets_buttons(self):
        for name, config in self.list_config_buttons.items():
            btn = Gtk.Button()
            btn.connect("clicked", config['function'])
            btn.set_label(config['label'])
            btn.set_visible(True)
            btn.set_focus_on_click(True)
            if config['icon']:
                btn.set_image(self.widget_images[config['icon']])
                btn.set_always_show_image(True)

            self.widget_buttons[name] = btn

    def create_widget_menu(self):
        
        self.widget_menu = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(self.widget_menu.get_style_context(), "linked")

        btn_select_file = Gtk.Button(label="Abrir arquivo")
        btn_select_file.connect("clicked", self.on_file_chooser)
        btn_select_file.set_image(self.widget_images['icon_open'])
        btn_select_file.set_always_show_image(True)

        self.widget_menu.add(btn_select_file)

        btn_open = Gtk.MenuButton()
        btn_open.add(Gtk.Arrow(Gtk.ArrowType.DOWN, Gtk.ShadowType.NONE))        

        menu = Gtk.Menu()
        btn_open.set_popup(menu)

        for label, function in self.list_config_menu.items():
            item = Gtk.MenuItem(label)
            item.connect("activate", function)
            menu.append(item)

        menu.show_all()

        self.widget_menu.add(btn_open)

    def create_layout(self):
        self.layout = Gtk.Grid()
        self.layout.set_row_spacing(4)

        self.widget_switch_graph = self.get_widget_switch()
        self.widget_switch_edit  = self.get_widget_switch(False, self.on_notify_enable_edit)

        # ------------ Header -------------------
        header = Gtk.HeaderBar()
        header.set_hexpand(True)
        # Left widgets
        header.pack_start(self.widget_menu)
        header.pack_start(self.get_widget_separator(VERTICAL))
        header.pack_start(self.widget_switch_graph)
        header.pack_start(self.get_widget_label("Mostrar grafo"))
        
        # Right widgets
        header.pack_end(self.widget_buttons['btn_help'])
        header.pack_end(self.get_widget_separator(VERTICAL))
        header.pack_end(self.widget_switch_edit)
        header.pack_end(self.get_widget_label("Habilitar edição"))

        # --------- Pager ------------------
        self.widget_pager = Gtk.Stack()
        # Buttons for change page
        widget_pager_switcher = Gtk.StackSwitcher()
        widget_pager_switcher.set_stack(self.widget_pager)
        widget_pager_switcher.set_halign(Gtk.Align.CENTER)

        # Creator pages
        page_output, self.buffer_output                    = self.get_page_output()
        page_editor, self.buffer_code, self.widget_editor  = self.get_page_editor()
        
        self.widget_pager.add_titled(page_editor, 'page_code', "Editor")
        self.widget_pager.add_titled(page_output, 'page_output', "Saida")

        # --------- Buttons actions --------------
        
        widget_action_bar = Gtk.ActionBar()

        self.label_filename = self.get_widget_label("Selecione um arquivo")

        widget_action_bar.pack_start(self.widget_buttons['btn_lexer'])
        widget_action_bar.pack_start(self.get_widget_separator(VERTICAL))
        widget_action_bar.pack_start(self.widget_buttons['btn_syntax'])
        widget_action_bar.pack_start(self.get_widget_separator(VERTICAL))
        widget_action_bar.pack_start(self.widget_buttons['btn_semantic'])
        widget_action_bar.pack_start(self.get_widget_separator(VERTICAL))
        widget_action_bar.pack_start(self.widget_buttons['btn_generator'])

        widget_action_bar.pack_end(self.widget_buttons['btn_clear'])
        widget_action_bar.pack_end(self.get_widget_separator(VERTICAL))
        widget_action_bar.pack_end(self.label_filename)

        self.buffer_mepa = Gtk.TextBuffer()

        self.layout.attach(header,                0, 0, 1, 1)
        self.layout.attach(widget_pager_switcher, 0, 1, 1, 1)
        self.layout.attach(self.widget_pager,     0, 2, 1, 1)
        self.layout.attach(widget_action_bar,     0, 3, 1, 1)

    def get_widget_separator(self, orientation):
        return Gtk.Separator.new(Gtk.Orientation(orientation))

    def get_widget_switch(self, activate = True, signal = None):
        switch = Gtk.Switch()
        switch.set_state(activate)

        if signal:
            switch.connect("notify::active", signal)

        return switch

    def get_widget_label(self, label):
        return Gtk.Label.new(label)

    def get_page_output(self):
        widget_scrolled_window = Gtk.ScrolledWindow()
        widget_scrolled_window.set_hexpand(True)
        widget_scrolled_window.set_vexpand(True)

        screen_area = Gtk.TextView()
        screen_area.set_top_margin(10)
        screen_area.set_left_margin(10)
        screen_area.set_right_margin(10)
        screen_area.set_bottom_margin(10)
        screen_area.set_indent(4)
        screen_area.set_monospace(True)
        screen_area.set_editable(False)
        screen_area.set_accepts_tab(False)
        screen_area.set_cursor_visible(False)
        screen_area.set_wrap_mode(Gtk.WrapMode(2)) # wrap by word
        screen_area.set_name("text_output")

        buff = screen_area.get_buffer()

        widget_scrolled_window.add(screen_area)

        return widget_scrolled_window, buff

    def get_page_editor(self):
        widget_scrolled_window = Gtk.ScrolledWindow()
        widget_scrolled_window.set_hexpand(True)
        widget_scrolled_window.set_vexpand(True)

        editor  = GtkSource.View()
        buff    = editor.get_buffer()

        lm = GtkSource.LanguageManager()
        sm = GtkSource.StyleSchemeManager()
        
        paths_lang = lm.get_default().get_search_path()
        paths_lang.append(os.getcwd() + "/interface/editor/lang")
        lm.set_search_path(paths_lang)

        paths_scheme = sm.get_default().get_search_path()
        paths_scheme.append(os.getcwd() + "/interface/editor/style")
        sm.set_search_path(paths_scheme)

        buff.set_language(lm.get_language('cpascal'))
        buff.set_highlight_syntax(True)
        buff.set_style_scheme(sm.get_scheme('dark_cpascal'))
        
        editor.set_show_line_numbers(True)
        editor.set_auto_indent(True)
        editor.set_sensitive(False)
        editor.set_highlight_current_line(True)
        editor.set_monospace(True)
        editor.set_pixels_above_lines(2)
        editor.set_pixels_inside_wrap(2)
        editor.set_right_margin(2)
        editor.set_show_line_marks(True)
        editor.set_smart_backspace(True)
        editor.set_wrap_mode(Gtk.WrapMode(2))

        widget_scrolled_window.add(editor)

        return widget_scrolled_window, buff, editor

    def dialog_creator(self, title, main_text, flags = 0, type_message = Gtk.MessageType.WARNING, type_button = Gtk.ButtonsType.OK):
        dialog = Gtk.MessageDialog(self, flags,type_message,type_button,title)
        dialog.format_secondary_text(main_text)
        dialog.run()

        dialog.destroy()

    ########### STATES SWITCHERS ##############

    def is_show_graph(self):
        return self.widget_switch_graph.get_active()

    def is_edit_code(self):
        return self.widget_switch_edit.get_active()

    ########### SYSTEM FUNCTIONS ############

    def get_file_name(self):
        return self.file_path.split('/')[-1]

    def update_label_filename(self):
        self.label_filename.set_text("File: " + self.get_file_name())

    def load_file(self):

        with open(self.file_path) as file:
            content = file.read()

        self.update_label_filename()
        self.buffer_code.set_text(content)
        self.widget_pager.set_visible_child_name('page_code')

    def read_buffer(self, buff, include_hidden_chars = True):
        start, end, hidden = buff.get_start_iter(), buff.get_end_iter(), include_hidden_chars
        if self.file_path:
            return buff.get_text(start, end, hidden)
        
        return None

    def write_buffer(self, buff, input_text):
        old_text = self.read_buffer(buff)
        iterator = buff.get_end_iter()
        input_text += '\n' + "-"*100 + '\n\n' + old_text
        buff.set_text(input_text)

    def execute_phase(self, function, title_id, show_image = False):
        code = self.read_buffer(self.buffer_code)

        title = {
            "lexer": "Analizador lexico",
            "syntax": "Analizador sintatico",
            "semantic": "Analizador semantico"
        }

        if code:
            status, out = function(code)

            if status:
                write_screen = "--> %s (%s): \n\n" % (title[title_id], self.file_path) + str(out)
                
                if show_image:
                    Image = TreeImage()
                    Image.translate(str(out))
                    Image.view_graph()

                self.write_buffer(self.buffer_output, write_screen)
                self.widget_pager.set_visible_child_name('page_output')
            else:
                self.show_compile_error_dialog(out)
        else:
            self.show_dialog_select_file()

    def show_dialog_select_file(self):
        self.dialog_creator("Aviso", "Selecione um arquivo primeiro!")

    def show_compile_error_dialog(self, error):
        class_name, line, error_message = error.get_attr()
        line = line if not isinstance(line, tuple) else line[0] 
        message = "Line: %s \t %s" % (line, error_message)
        self.dialog_creator(class_name, message, type_message=Gtk.MessageType.ERROR)

    def save_mepa_dialog(self):

        dialog = Gtk.FileChooserDialog("Salvar arquivo MEPA", self,
            Gtk.FileChooserAction.SAVE,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        dialog.set_position(Gtk.Align.CENTER)

        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            self.mepa_file = dialog.get_filename()
            self.save_mepa_code()
    
        dialog.destroy()

    def save_mepa_code(self):
        if self.mepa_file:
            code = self.read_buffer(self.buffer_mepa)

            with open(self.mepa_file, 'w+') as file:
                file.writelines(code)
        else:
            self.save_mepa_dialog()


    ########### CALLBACKS FUNCTIONS ############
    def on_help_clicked(self, button):
        dialog = HelpDialog(self)
        dialog.run()
        dialog.destroy()

    def on_file_chooser(self, button):

        dialog = Gtk.FileChooserDialog("Favor selecione um arquivo", self,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        dialog.set_position(Gtk.Align.CENTER)

        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            self.file_path = dialog.get_filename()
            self.load_file()
            self.widget_switch_edit.set_active(True)
            self.widget_editor.set_sensitive(True)

        dialog.destroy()

    def on_file_save(self, *args):
        if self.file_path:
            code = self.read_buffer(self.buffer_code)
            
            with open(self.file_path, 'w+') as file:
                file.writelines(code)
        else:
            self.dialog_creator("Aviso", "Abra um arquivo antes de tentar salvar!")

    def on_notify_enable_edit(self, *args):
        if self.widget_switch_edit.get_active():
            if self.file_path:
                self.widget_editor.set_sensitive(True)
            else:
                self.dialog_creator("Aviso", "Selecione um arquivo antes de tentar editar")
                self.widget_switch_edit.set_active(False)
        else:
            self.widget_editor.set_sensitive(False)

    def on_clear_clicked(self, *args):
        self.buffer_output.set_text('')

    def on_lexer_clicked(self, *args):
        self.execute_phase(function=LexerG, title_id="lexer")

    def on_syntax_clicked(self, *args):
        self.execute_phase(function=SyntaxG, title_id="syntax", show_image=self.is_show_graph())

    def on_semantic_clicked(self, *args):
        self.execute_phase(function=SemanticG, title_id="semantic")
    
    def on_generator_clicked(self, *args):
        # Faz a execução
        code = self.read_buffer(self.buffer_code)

        if code:
            ast, out = SemanticG(code)

            if ast:

                self.save_mepa_code()

                win_mepa = Interpleter(self.mepa_file)
                win_mepa.show()

            else:
                self.show_compile_error_dialog(out)

        else:
            self.show_dialog_select_file()

if __name__ == '__main__':
    window = MainWindow()
    Gtk.main()