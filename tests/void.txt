void exemplo ()
   vars 
      z : integer;
   functions
      integer k (a : integer)
      {
         return a * a + 1;
      }
      integer h (m, p : integer; var x : integer)
         vars 
            z : integer;
         functions
            integer g (j : integer)
               vars 
                  s, z : integer;
            {
               s = k(j);
               if (k(p) >= s) {
                  g = 1; 
                  x = x + 1;
               }
               else
               {
                  g = h(m-1, p-s, z); 
                  x = x + z;
               }
            }
      {
         if (m == 1) {
            h = 1; 
            x = 1;
         }
         else
         {
            x = 0; 
            h = h(m-1, p, z) + p * g(2); 
            x = x + z;
         }
      }

      
{
   write(h(1, 10, z), z);
}