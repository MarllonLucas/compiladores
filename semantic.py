#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=assignment-from-no-return

from collections import deque
from itertools import chain

from libs.errors import CompilerError, LexicalError, SemanticError
from libs.misc import AST

from lexical import build as build_lexer
from syntax import build as build_parser
from typing import Iterable, Optional, Tuple

import libs.symboltable as st
import sys

class ActionRouter:
    """
    Every method in the form `_a_something` is an Action whose name is
    'something'. This class is intended to map strings to methods.
    So if you have an sequence of strings telling you what actions/methods
    must be done/called, there is no need to use an if-elif-else chain.
    """

    def call_action(self, action):
        """
        Execute an action for an `AST`, only if `action` is an `AST`.
        Exclusive to AST types.
        """
        if isinstance(action, AST):
            return self.get_action(action.name)(action)

    def get_action(self, action_name):
        "Returns a bounded method to an action of name `action_name`"
        return self.__getattribute__('_a_' + action_name)

class SemanticAnalyzer(ActionRouter):
    '''
    Parser that fills the symbol table with every definition that appears in
    an AST. Note that references (not definitions) will be skipped.
    '''

    def __init__(self, ast,initial_symbol_table=st.SYMBOL_TABLE):
        self.symboltable = initial_symbol_table
        self._context = self.symboltable  # global
        self._context_function = None
        self.ast = ast

    def _update_context(self, context: st.SymbolTable):
        self._context = context

    def _pop_context(self) -> st.SymbolTable:
        c = self._context
        self._context = c.parent
        return c

    def annotate(self):
        return self.get_symbol_table()

    def get_symbol_table(self):
        'Retrieve all symbols inside an AST'
        # Reset context
        self._context = self.symboltable
        # Start
        self._a_program(self.ast)
        return self.symboltable

    def _a_program(self, expr: AST):
        '(program function)'
        assert expr.name == 'program'
        self._a_function(expr.args[0])

    def _a_function(self, expr: AST):
        '(function function_return:str identifier:str formal_parameters block)'
        assert expr.name == 'function'

        function_return, identifier, formal_parameters, block = expr.args

        # Determine result type
        if function_return == 'void':
            result = None
        else:
            result = self._context.get(function_return)

            if result is None:
                raise SemanticError(
                    0, "Type '%s' not defined" % function_return)
            elif not isinstance(result[0], st.Type):
                raise SemanticError(
                    0, "Symbol '%s' is not a type" % function_return)

            result = result[0]

        if result and result.is_array():
            raise SemanticError(expr.pos, "Function must not return arrays")

        # Determine parameters type
        params = () if not formal_parameters \
            else tuple(self._a_formal_parameters(formal_parameters))

        # Register function definition on symbol table
        function = st.Function(
            identifier, type=st.FunctionType(result, params))
        # Register with a context creation
        new_context = self._context.add_with_context(function)

        for param in params:
            for i in self.iter_params(param):
                n, t, r = i.name, i.descriptor.typedesc, i.descriptor.by_reference
                new_context.add(st.Variable(n, typedesc=t, reference=r))

        # update function context
        self._context_function = function

        # Block evaluation must be inside the context of this function
        self._update_context(new_context)
        self._a_block(block)  # Evaluate block
        self._pop_context()

    def iter_params(self, params):
        for i in params:
            yield i

    def _a_block(self, expr: AST):
        '(block labels_e types_e variables_e functions_e body)'
        assert expr.name == 'block'

        labels, types, variables, functions, body = expr.args
        if labels:
            self._a_labels(labels)
        if types:
            self._a_types(types)
        if variables:
            self._a_variables(variables)
        if functions:
            self._a_functions(functions)

        self._a_body(body)

    def _a_labels(self, expr: AST):
        assert expr.name == 'labels'
        # These labels are not definitions, only declarations that they exist.
        # Thus, they do not hold any address.
        # So we have nothing to do here.
        # XXX Maybe we could throw a warning if a label is declared here but not used.
        for i in expr.args:
            for label in self._a_identifier_list(i):
                label_obj = st.Label(label)
                self._context.add(label_obj)

    def _a_types(self, expr: AST) -> Optional[str]:
        assert expr.name == 'types'

        for type_alias in expr.args:
            t_alias, t_type = self._a_types_alias(type_alias)
            t_type = self._a_type(t_type)  # return type object

            symbol = st.Type(t_alias, t_type)  # create a new type
            self._context.add(symbol)  # add on my symble table

    def _a_types_alias(self, expr: AST) -> Optional[str]:
        assert expr.name == 'typealias'
        return expr.args

    def _a_variables(self, expr: AST) -> Optional[str]:
        assert expr.name == 'variables'
        for expr_var in expr.args:
            identifier_list, type_var = expr_var.args

            list_vars = self._a_identifier_list(identifier_list)
            t_var = self._a_type(type_var)

            for var in list_vars:
                symbol = st.Variable(var, typedesc=t_var)
                self._context.add(symbol)

    def _a_functions(self, expr: AST) -> Optional[str]:
        assert expr.name == 'functions'
        for function in expr.args:
            self._a_function(function)

    def _a_body(self, expr: AST) -> Optional[str]:
        assert expr.name == 'body'
        for statement in expr.args:
            self._a_statement(statement)

    def _a_type(self, expr: AST) -> Optional[str]:
        assert expr.name == 'type'

        obj_type = self._context.get(expr.args[0])

        if not obj_type:
            raise SemanticError(expr.pos, "Type '%s' is not defined" % expr.args[0])

        obj_type, context = obj_type
        
        size_array = len(expr.args) - 1

        if size_array:
            if(not obj_type.is_array()):
                type_desc = st.Array(size_array, obj_type.get_descriptor())
                obj_type = st.Type(expr.args[0], type_desc)
            else:
                raise SemanticError(expr.pos, "It is not possible to create an array for the '%s'" % expr.args[0])

        return obj_type

    def _a_type_l0(self, expr: AST) -> Optional[str]:
        pass

    def _a_formal_parameters(self, expr: AST) -> Iterable[st.Parameter]:
        '(formal_parameters {expression_parameter|function_parameter}...)'
        assert expr.name == 'formal_parameters'

        return (self.call_action(param) for param in expr.args) 

    def _a_expression_parameter(self, expr: AST) -> Iterable[st.Parameter]:
        '(expression_parameter by_reference:bool identifier_list type)'
        assert expr.name == 'expression_parameter'

        by_reference, id_list, types = expr.args
        typedesc = self._a_identifier(types, expr.pos)
            
        parameters = []

        for i in self._a_identifier_list(id_list):
            parameters.append(st.Parameter(i, st.ParamDescriptor(typedesc, by_reference)))

        return parameters

    def _a_function_parameter(self, expr: AST) -> Iterable[st.Parameter]:
        assert expr.name == 'function_parameter'
        raise NotImplementedError(expr.name)

    def _a_statement(self, expr: AST) -> Optional[str]:
        assert expr.name == 'statement'
        label, state = expr.args
        
        if label:
            l = self._context.get(label)

            if not l:
                raise SemanticError(expr.pos, "Label '%s' is not defined" % label)

            if not isinstance(l[0], st.Label):
                raise SemanticError(expr.pos, "The '%s' is not label" % label)

        self.call_action(state)

    def _a_nop(self, expr: AST) -> Optional[str]:
        assert expr.name == 'nop'
        pass

    def _a_variable(self, expr: AST) -> Optional[str]:
        assert expr.name == 'variable'
        name, args = expr.args
        var = self._context.get(name)

        if not var:
            raise SemanticError(
                expr.pos, "Variable '%s' is not defined" % name)

        var, context = var

        if not isinstance(var, st.Variable):
            raise SemanticError(expr.pos, "The '%s' is not variable" % name)

        if (var.typedesc.is_array()):

            if not len(args):
                raise SemanticError(expr.pos, "The variable '%s' is an array, to access it you must pass the matrix parameter" % name)

            dimention = var.typedesc.get_array().size
            size_args = len(args)
            
            if dimention != size_args:
                raise SemanticError(expr.pos, "Invalid array access")

            for i in range(size_args):
                type_param = self.call_action(args[i])

                if type_param != st.Symbols.INTEGER:
                    raise SemanticError(expr.pos, "Invalid expression for matrix access")

            # retornar o tipo primitivo deste objeto caso o mesmo atendeu a todas as exigencias
            return var.typedesc.get_array().element_type

        return var.typedesc

    def _a_assignment(self, expr: AST) -> Optional[str]:
        assert expr.name == 'assignment'
        variable, expression = expr.args
        type_variable = self._a_variable(variable)
        expression = self._a_expression(expression)

        if not self.compare_type_equals([type_variable, expression]):
            raise SemanticError(expr.pos, "Incompatible types")

    def _a_function_call_statement(self, expr: AST) -> Optional[str]:
        assert expr.name == 'function_call_statement'
        for i in expr.args:
            self.call_action(i)

    def _a_goto(self, expr: AST) -> Optional[str]:
        assert expr.name == 'goto'
        type_goto = self._a_identifier(expr.args[0], expr.pos)
        
        if(not isinstance(type_goto, st.Label)):
            raise SemanticError(
                expr.pos, "Invalid '%s' label" % expr.args[0])

    def _a_identifier(self, key: str, pos) -> Optional[str]:
        symbol = self._context.get(key)

        if not symbol:
            raise SemanticError(pos, "Identifier '%s' is not defined" % key)

        return symbol[0]

    def _a_return(self, expr: AST) -> Optional[str]:
        assert expr.name == 'return'
        expression = expr.args[0]
        types = None

        if expression:
            types = self._a_expression(expression)

        name_function   = self._context_function.name
        result_function = self._context_function.type.result

        return_type = result_function.name if result_function else None        

        if not (result_function == types):
            raise SemanticError(expr.pos, "The function '%s' must return the type '%s'" % (
                name_function, return_type))

    def _a_compound(self, expr: AST) -> Optional[str]:
        assert expr.name == 'compound'
        for i in expr.args:
            self.call_action(i)

    def _a_conditional(self, expr: AST) -> Optional[str]:
        assert expr.name == 'conditional'
        expression, compound, conditional_else = expr.args
        type_expr = self._a_expression(expression)

        if type_expr != st.Symbols.BOOLEAN:
            raise SemanticError(
                expr.pos, "Expression does'snt return Boolean type")

        self._a_compound(compound)
        if conditional_else:
            self._a_conditional_else(conditional_else)

    def _a_conditional_else(self, expr: AST) -> Optional[str]:
        assert expr.name == 'conditional_else'
        for i in expr.args:
            self.call_action(i)

    def _a_repetitive(self, expr: AST) -> Optional[str]:
        assert expr.name == 'repetitive'
        expression, compound = expr.args

        type_expr = self._a_expression(expression)

        if type_expr != st.Symbols.BOOLEAN:
            raise SemanticError(
                expr.pos, "Expression does'snt return Boolean type")

        self._a_compound(compound)

    def _a_expression(self, expr: AST, retrive=False) -> Optional[str]:
        assert expr.name == 'expression'
        types_expr = None
        for expression in expr.args:
            if expression.name == 'simple_expression': 
                types_expr = self._a_simple_expression(expression, retrive=retrive)
            else:
                types_expr = self.call_action(expression)

        return types_expr

    def _a_simple_expression(self, expr: AST, retrive=False) -> Optional[str]:
        assert expr.name == 'simple_expression'
        unary_op, term, simple_expression_l0 = expr.args
        types_expr = []
        types_expr.append(self._a_term(term, retrive=retrive and not simple_expression_l0))
        if simple_expression_l0:
            types_expr.append(
                self._a_simple_expression_l0(simple_expression_l0))

        if not self.compare_type_equals(types_expr):
            raise SemanticError(expr.pos, "Incompatible types")

        return types_expr[0]

    def _a_simple_expression_l0(self, expr: AST) -> Optional[str]:
        assert expr.name == 'simple_expression_l0'
        additive_operator, term, simple_expression_l0 = expr.args
        types_expr = []
        types_expr.append(self._a_term(term))
        if simple_expression_l0:
            types_expr.append(
                self._a_simple_expression_l0(simple_expression_l0))

        if not self.compare_type_equals(types_expr):
            raise SemanticError(expr.pos, "Incompatible types")

        return types_expr[0]

    def _a_term(self, expr: AST, retrive=False) -> Optional[str]:
        assert expr.name == 'term'
        types_terms = []

        factor, term_l0 = expr.args
        types_terms.append(self._a_factor(factor, retrive=(retrive and not term_l0)))

        if term_l0:
            types_terms.append(self._a_term_l0(term_l0))

        if not self.compare_type_equals(types_terms):
            raise SemanticError(expr.pos, "Incompatible types")

        return types_terms[0]

    def _a_term_l0(self, expr: AST) -> Optional[str]:
        assert expr.name == 'term_l0'
        operator, factor, term_l0 = expr.args
        types_terms = []
        types_terms.append(self._a_factor(factor))
        if term_l0:
            types_terms.append(self._a_term_l0(term_l0))

        if not self.compare_type_equals(types_terms):
            raise SemanticError(expr.pos, "Incompatible types")

        return types_terms[0]

    def _a_multiplicative_operator(self, expr: AST) -> Optional[str]:
        assert expr.name == 'multiplicative_operator'
        return expr.args

    def _a_factor(self, expr: AST, retrive=False) -> Optional[str]:
        assert expr.name == 'factor'
        type_factor = []

        for i in expr.args:
            type_factor.append(self.call_action(i))

        if not self.compare_type_equals(type_factor):
            raise SemanticError(expr.pos, "Incompatible types")

        if retrive:
            if expr.args[0].name == 'variable':
                return expr.args[0].args[0]

        return type_factor[0]

    def compare_type_equals(self, list_types):
        equals = True

        for i in range(1, len(list_types)):
            if(not (list_types[0] == list_types[i])): return False

        return True

    def _a_not(self, expr: AST) -> Optional[str]:
        assert expr.name == 'not'
        for i in expr.args:
            return self.call_action(i)

    def _a_function_call(self, expr: AST) -> Optional[str]:
        assert expr.name == 'function_call'
        function_name, expression_list = expr.args

        function = self._a_identifier(function_name, expr.pos)

        if function_name not in ('read', 'write'):
            list_param_type = []

            for param in function.type.parameters:
                for i in self.iter_params(param):
                    list_param_type.append((i.descriptor.typedesc, i.descriptor.by_reference)) 

            expr_list = []

            if expression_list:
                expr_list = self._a_expression_list(expression_list, retrive=True)


            if len(expr_list) != len(list_param_type):
                raise SemanticError(expr.pos, "Number of params invalid.")

            expr_types_var = []

            for exp in expr_list:

                flag = False
                var = exp

                if isinstance(exp, str):
                    s, _ = self._context.get(exp)

                    if isinstance(s, st.Variable):
                        var, flag = s.typedesc, True

                expr_types_var.append((var, flag))

            for i in range(len(list_param_type)):
                if list_param_type[i][1]:
                    if not expr_types_var[i][1]:
                        raise SemanticError(expr.pos, "Invalid parameters")

                    if not (expr_types_var[i][0] == list_param_type[i][0]):
                        raise SemanticError(expr.pos, "Invalid parameters")
                else:    
                    if not (expr_types_var[i][0] == list_param_type[i][0]):
                        raise SemanticError(expr.pos, "Invalid parameters")

        return function.type.result

    def _a_identifier_function(self, key: str) -> Optional[str]:
        search_identifier = self._context.get(key)

        if not search_identifier:
            raise SemanticError(-1, "Identifier '%s' is not defined" % key)

        return search_identifier[0].type

    def _a_identifier_list(self, expr: AST) -> Tuple[str]:
        assert expr.name == 'identifier_list'
        return expr.args

    def _a_expression_list(self, expr: AST, retrive=False) -> Optional[str]:
        assert expr.name == 'expression_list'
        types_expr = []
        for expression in expr.args:
            types_expr.append(self._a_expression(expression, retrive=retrive))

        return types_expr

    def _a_eq(self, expr: AST) -> Optional[str]:
        assert expr.name == 'eq'
        types = []

        for i in expr.args:
            types.append(self.call_action(i))

        if not self.compare_type_equals(types):
            raise SemanticError(expr.pos, "Incompatible types")

        return st.Symbols.BOOLEAN

    def _a_le(self, expr: AST) -> Optional[str]:
        assert expr.name == 'le'
        types = []

        for i in expr.args:
            types.append(self.call_action(i))

        if not self.compare_type_equals(types):
            raise SemanticError(expr.pos, "Incompatible types")

        return st.Symbols.BOOLEAN

    def _a_ge(self, expr: AST) -> Optional[str]:
        assert expr.name == 'ge'
        types = []

        for i in expr.args:
            types.append(self.call_action(i))

        if not self.compare_type_equals(types):
            raise SemanticError(expr.pos, "Incompatible types")

        return st.Symbols.BOOLEAN

    def _a_ne(self, expr: AST) -> Optional[str]:
        assert expr.name == 'ne'
        types = []

        for i in expr.args:
            types.append(self.call_action(i))

        if not self.compare_type_equals(types):
            raise SemanticError(expr.pos, "Incompatible types")

        return st.Symbols.BOOLEAN

    def _a_lt(self, expr: AST) -> Optional[str]:
        assert expr.name == 'lt'
        types = []

        for i in expr.args:
            types.append(self.call_action(i))

        if not self.compare_type_equals(types):
            raise SemanticError(expr.pos, "Incompatible types")

        return st.Symbols.BOOLEAN

    def _a_gt(self, expr: AST) -> Optional[str]:
        assert expr.name == 'gt'
        types = []

        for i in expr.args:
            types.append(self.call_action(i))

        if not self.compare_type_equals(types):
            raise SemanticError(expr.pos, "Incompatible types")

        return st.Symbols.BOOLEAN

    def _a_integer(self, expr: AST) -> Optional[str]:
        assert expr.name == 'integer'
        return st.Symbols.INTEGER

def build(data):
    return SemanticAnalyzer()

def semantic_graphic(data):
    try:
        ast = build_parser().parse(data, lexer=build_lexer(), tracking=True)
        return ast, SemanticAnalyzer(ast).annotate()
    except Exception as e:
        return False, e
   
def main():
    data = sys.stdin.read()

    ast = build_parser().parse(data, lexer=build_lexer(), tracking=True)
    semantic = SemanticAnalyzer(ast)

    try:
        print(semantic.annotate())
    except Exception as e:
        raise e

if __name__ == "__main__":
    try:
        main()
    except CompilerError as e:
        print(e, file=sys.stderr)
        exit(1)
