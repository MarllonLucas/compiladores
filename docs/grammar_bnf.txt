# grammar bnf

program : function
function : function_return identifier formal_parameters block
    function_return : identifier | VOID
block : labels_e types_e variables_e functions_e body
    labels_e : labels | empty
    types_e : types | empty
    variables_e : variables | empty
    functions_e : functions | empty
labels : LABELS identifier_list SEMICOL
types : TYPES types_l1
    types_l1 : identifier EQ type SEMICOL types_l0
    types_l0 : identifier EQ type SEMICOL types_l0 | empty
variables : VARS variables_l1
    variables_l1 : identifier_list COL type SEMICOL variables_l0
    variables_l0 : identifier_list COL type SEMICOL variables_l0 | empty
functions : FUNCTIONS functions_l1
    functions_l1 : function functions_l0
    functions_l0 : function functions_l0 | empty
body : LBRACE body_l0 RBRACE
    body_l0 : statement body_l0 | empty
type : identifier type_l0
    type_l0 : LBRACK integer RBRACK type_l0 | empty
formal_parameters : LPAREN formal_parameters_i RPAREN
    formal_parameters_i : formal_parameter formal_parameters_l0 | empty
    formal_parameters_l0 : SEMICOL formal_parameter formal_parameters_l0 | empty
formal_parameter : expression_parameter | function_parameter
expression_parameter : VAR identifier_list COL identifier
                     |     identifier_list COL identifier
function_parameter : function_return identifier formal_parameters
statement : identifier COL unlabeled_statement | unlabeled_statement | compound
variable : identifier variable_l0
    variable_l0 : LBRACK expression RBRACK variable_l0 | empty
unlabeled_statement : assignment | function_call_statement | goto | return
                    | conditional | repetitive | empty_statement
assignment : variable EQ expression SEMICOL
function_call_statement : function_call SEMICOL
goto : GOTO identifier SEMICOL
return : RETURN expression_e SEMICOL
    expression_e : expression | empty
compound : LBRACE compound_l1 RBRACE
    compound_l1 : unlabeled_statement compound_l0
    compound_l0 : unlabeled_statement compound_l0 | empty
conditional : IF LPAREN expression RPAREN compound conditional_else
    conditional_else : ELSE compound | empty
repetitive : WHILE LPAREN expression RPAREN compound
empty_statement : SEMICOL
expression : simple_expression expression_i
    expression_i : relational_operator simple_expression | empty
relational_operator : RELOP
simple_expression : PLUS  term simple_expression_l0
                  | MINUS term simple_expression_l0
                  | term simple_expression_l0
    simple_expression_l0 : additive_operator term simple_expression_l0 | empty
additive_operator : PLUS | MINUS | OR
term : factor term_l0
    term_l0 : multiplicative_operator factor term_l0 | empty
multiplicative_operator : TIMES | DIVIDE | EE
factor : variable | integer | function_call | LPAREN expression RPAREN | NOT factor
function_call : identifier LPAREN expression_list RPAREN
identifier_list : identifier identifier_list_l0
    identifier_list_l0 : COMMA identifier identifier_list_l0 | empty
expression_list : expression expression_list_l0 | empty
    expression_list_l0 : COMMA expression expression_list_l0 | empty
empty :


integer : NUMBER
identifier : IDENTIFIER

# NOTE: we have these productions as tokens, then they are not needed
# integer : digit integer_l0
#     integer_l0 : digit integer_l0 | empty
# identifier : letter identifier_l0
#     identifier_l0 : letter identifier_l0 | digit identifier_l0 | empty
# letter : a-z
# digit : 0-9
