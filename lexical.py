#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Extrator de átomos da linguagem Simple.
"""

import sys

from ply import lex
from libs.errors import CompilerError, LexicalError

CASE_INSENSITIVE = True

# Lista de átomos. Essa declaração é necessária.
tokens = (
    'RELOP',
    'EE',
    'OR',
    'SEMICOL',
    'COL',
    'EQ',
    'COMMA',
    'NOT',

    'NUMBER',
    'PLUS',
    'MINUS',
    'TIMES',
    'DIVIDE',
    'LPAREN',
    'RPAREN',
    'LBRACK',
    'RBRACK',
    'LBRACE',
    'RBRACE',

    # Identifiers and reserved words.
    'IDENTIFIER',
    "BOOLEAN",
    "ELSE",
    "FALSE",
    "GOTO",
    "IF",
    "FUNCTIONS",
    "INTEGER",
    "LABELS",
    "READ",
    "RETURN",
    "TRUE",
    "TYPES",
    "VAR",
    "VARS",
    "VOID",
    "WHILE",
    "WRITE"
)

# Expressões regulares para reconhecimento de átomos simples
t_PLUS    = r'\+'
t_MINUS   = r'-'
t_TIMES   = r'\*'
t_DIVIDE  = r'/'
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
t_LBRACK  = r"\["
t_RBRACK  = r"\]"
t_LBRACE  = r"\{"
t_RBRACE  = r"\}"
t_EE      = r"&&"
t_OR      = r"\|\|"
t_SEMICOL = r";"
t_COL     = r":"
t_EQ      = r"="
t_COMMA   = r","
t_NOT     = r"!"
t_ignore  = ' \t' # Cadeia que contem carcateres ignorados (espacos and tabulacao)

# Maintain sorted.
reserved = {
    "boolean":"BOOLEAN",
    "else":"ELSE",
    "false":"FALSE",
    "functions":"FUNCTIONS",
    "goto":"GOTO",
    "if":"IF",
    "integer":"INTEGER",
    "labels":"LABELS",
    "read":"READ",
    "return":"RETURN",
    "true":"TRUE",
    "types":"TYPES",
    "var":"VAR",
    "vars":"VARS",
    "void":"VOID",
    "while":"WHILE",
    "write":"WRITE"
}

def t_RELOP(t):
    r'==|<=|>=|!=|<|>'
    return t

def t_IDENTIFIER(t):
    r'[A-Za-z][A-Za-z\d]*'
    if CASE_INSENSITIVE:
        t.value = t.value.lower()
    t.type = reserved.get(t.value, 'IDENTIFIER')
    return t

# Uma expressão regular para reconhecimento de números
# já exige código adicional de tratamento
def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        raise LexicalError(t.lineno, "Number %s is too large!" % (t.value))
    return t

# Define uma regra para controle do numero de linhas
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# Whitespace
def t_WHITESPACE(t):
    r'\s+'
    t.lexer.lineno += t.value.count('\n')

# Comment block
def t_BlockComment(t):
    r'/\*(.|\n)*?\*/'
    t.lexer.lineno += t.value.count('\n')

# Comment line
def t_LineComment(t):
    r'//.*\n'
    t.lexer.lineno += 1

# Regra para manipulação de erros
def t_error(t):
    raise LexicalError(t.lineno, "Illegal character '%s'" % t.value[0])
    # t.lexer.skip(1)

def build():
    return lex.lex()

def lexer_graphic(dados):
    try:
        lexer = lex.lex() # Constrói o analisador léxico

        # Chama o analisador, passando os dados como entrada
        lexer.input(dados)

        output = ''

        # Extrai os tokens
        while True:
            tok = lexer.token()
            if not tok: break # Final de arquivo
            output += "{:3} {:16} {}".format(tok.lineno, tok.type, tok.value) + "\n"

        return True, output
    
    except Exception as e:
        return False, e
    
def main():
    lexer = lex.lex() # Constrói o analisador léxico

    # Abre o arquivo de entrada e faz a leitura do texto
    # with open("teste.txt", "r") as ref_arquivo:
    #     dados = ref_arquivo.read()
    dados = sys.stdin.read()

    # Chama o analisador, passando os dados como entrada
    lexer.input(dados)

    # Extrai os tokens
    while True:
        tok = lexer.token()
        if not tok: break # Final de arquivo
        print("{:3} {:16} {}".format(tok.lineno, tok.type, tok.value))

if __name__ == "__main__":
    try:
        main()
    except CompilerError as e:
        print(e, file=sys.stderr)
