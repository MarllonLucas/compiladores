#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class CreatorStack(object):
	"""
		Classe responsavel por pegar um texto no formato de uma arvore sintatica e
		gerar uma estrutura de pilhas.

		Format text: 
			[node1 param .. [node2 param [node3 param]] [node4 param ..]]

		return 'get_stack':
			list = [('1', 'node1') ('2','param') .. [..] [..]]
	"""
	def __init__(self, text, identifiers=['+', '-', '*', '/']):
		super(CreatorStack, self).__init__()
		self.text = text
		self.identifiers = identifiers
		self.stack = []
		self.open_stack = '['
		self.close_stack = ']'

		self.create_stack()

	def is_identifier(self, char):
		return char.isalpha() or char.isnumeric() or char in self.identifiers

	def is_open_stack(self, char):
		return char == self.open_stack

	def is_close_stack(self, char):
		return char == self.close_stack

	def is_end_identifier(self, char):
		return char.isspace() or char == self.close_stack

	def is_text_iterable(self, content, iterator):
		return len(content) > iterator

	def get_identifier(self, content):
		identifier = ""
		iterator = 0

		while(not self.is_end_identifier(content[iterator])):
			identifier += content[iterator]
			iterator += 1

		return identifier, iterator

	def add_node(self, stack, content, id_node, iterator):
		identifier, it = self.get_identifier(content[iterator:])
		stack.append((str(id_node), identifier))

		iterator += it
		id_node += 1

		return iterator, id_node

	def add_stack(self, stack, content, id_node, iterator):
		iterator += 1

		new_stack, node_id, it = self.search_stacks(content[iterator:], id_node)

		if new_stack:
			stack.append(new_stack)

		iterator += it
		id_node = node_id

		return iterator, id_node

	def create_stack(self):

		iterator 	= 1
		id_node 	= 0
		
		while(self.is_text_iterable(self.text, iterator)):
			increment_counter = True

			if self.is_identifier(self.text[iterator]):
				iterator, id_node = self.add_node(self.stack, self.text, id_node, iterator)
				increment_counter = False

			if self.is_open_stack(self.text[iterator]):
				iterator, id_node = self.add_stack(self.stack, self.text, id_node, iterator)
				increment_counter = False

			if increment_counter:
				iterator += 1


	def search_stacks(self, content, id_node):
		stack = []
		iterator = 0

		while (self.is_text_iterable(content, iterator) and not self.is_close_stack(content[iterator])):
			increment_counter = True

			if self.is_identifier(content[iterator]):
				iterator, id_node = self.add_node(stack, content, id_node, iterator)
				increment_counter = False

			if self.is_open_stack(content[iterator]):
				iterator, id_node = self.add_stack(stack, content, id_node, iterator)
				increment_counter = False

			if increment_counter:
				iterator += 1

		iterator += 1

		return stack, id_node, iterator

	def get_stack(self):
		return self.stack
		
def main():

	# text = "[program [function void main () [block [labels [identifier_list l0 l1]] [types [typealias integral [type integer]] [typealias bool [type boolean]]] [variables [identifier_list m n s] [type integer] [identifier_list v t] [type integer [integer 2] [integer 3] [integer 4]]] () [body [statement () [assignment [variable m ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [assignment [variable n ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [assignment [variable s ()] [expression [simple_expression + [term [factor [variable m ()]] ()] [simple_expression_l0 + [term [factor [variable n ()]] ()] ()]]]]] [statement () [function_call_statement [function_call write [expression_list [expression [simple_expression + [term [factor [variable s ()]] ()] ()]]]]]] [statement () [return ()]]]]]]"

	text = "[program [function void hugefunction () [block () () () [functions [function void example1 () [block () () [variables [var [identifier_list m n s] [type integer]]] () [body [statement () [function_call_statement [function_call read [expression_list [expression [simple_expression + [term [factor [variable m ()]] ()] ()]] [expression [simple_expression + [term [factor [variable n ()]] ()] ()]]]]]] [statement () [assignment [variable s ()] [expression [simple_expression + [term [factor [integer 0]] ()] ()]]]] [statement () [repetitive [expression [le [simple_expression + [term [factor [variable m ()]] ()] ()] [simple_expression + [term [factor [variable n ()]] ()] ()]]] )]]]]] [function void example4 () [block () () [variables [var [identifier_list m] [type integer]]] [functions [function integer f [formal_parameters [expression_parameter False [identifier_list n] integer] [expression_parameter True [identifier_list k] integer]] [block () () [variables [var [identifier_list p q t] [type integer]]] () [body [statement () [conditional [expression [lt [simple_expression + [term [factor [variable n ()]] ()] ()] [simple_expression + [term [factor [integer 2]] ()] ()]]] [compound [assignment [variable k ()] [expression [simple_expression + [term [factor [integer 0]] ()] ()]]] [return [expression [simple_expression + [term [factor [variable n ()]] ()] ()]]]] [conditional_else [compound [assignment [variable t ()] [expression [simple_expression + [term [factor [function_call f [expression_list [expression [simple_expression + [term [factor [variable n ()]] ()] [simple_expression_l0 - [term [factor [integer 1]] ()] ()]]] [expression [simple_expression + [term [factor [variable p ()]] ()] ()]]]]] ()] [simple_expression_l0 + [term [factor [function_call f [expression_list [expression [simple_expression + [term [factor [variable n ()]] ()] [simple_expression_l0 - [term [factor [integer 2]] ()] ()]]] [expression [simple_expression + [term [factor [variable q ()]] ()] ()]]]]] ()] ()]]]] [assignment [variable k ()] [expression [simple_expression + [term [factor [variable p ()]] ()] [simple_expression_l0 + [term [factor [variable q ()]] ()] [simple_expression_l0 + [term [factor [integer 1]] ()] ()]]]]] [return [expression [simple_expression + [term [factor [variable t ()]] ()] ()]]]]]]]]]]] [body [statement () [function_call_statement [function_call write [expression_list [expression [simple_expression + [term [factor [function_call f [expression_list [expression [simple_expression + [term [factor [integer 3]] ()] ()]] [expression [simple_expression + [term [factor [variable m ()]] ()] ()]]]]] ()] ()]] [expression [simple_expression + [term [factor [variable m ()]] ()] ()]]]]]]]]]] [body [statement () [function_call_statement [function_call example1 ()]]] [statement () [function_call_statement [function_call example4 ()]]]]]]]"

	identifiers = ['+', '(', ')']

	staker = CreatorStack(text)
	stack = staker.get_stack()

	print(stack)




if __name__ == '__main__':
	main()