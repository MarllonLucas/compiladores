#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definições de classes erros para a Simple Language.
"""

class CompilerError(Exception):
    def __init__(self, pos, message):
        self.message = message
        self.pos = pos
        self.line = pos

        if isinstance(self.pos, tuple):
            self.line = self.pos[0]

        self.class_name = self.__class__.__name__

    def __repr__(self):
        return "{}(lineno={}, \"{}\")".format(
            self.class_name,
            self.line,
            self.message)
    
    def __str__(self):
        return repr(self)

    def get_attr(self):
        return self.class_name, self.pos, self.message

class LexicalError(CompilerError):
    pass

class SemanticError(CompilerError):
    pass