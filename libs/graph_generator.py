#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from graphviz import Digraph
from .creator_stack import CreatorStack

class TreeImage(object):
	"""
		libs necessarias:
			graphviz

			pip3 install graphviz

		Classe responsavel por fazer a traducao da saida o parser para
		a notacao da linguagem dot para a obtencao de uma imagem que representa
		a arvore

			@params path_export: [string], nome e caminho do arquivo para a exportacao
					format_export: [string] formato do tipo string da saida do arquivo

		Como usar a classe

			from graph_generator import TreeImage

			Image = TreeImage(path_export, format_export='png')
			# Jogue a saida do parser, DESDE QUE O MESMO NAO TENHA GERADO ERRO
			Image.translate(output_parser)

			# Caso quera mostar a imagem na tela
			Image.view_graph()

			# Caso queira exportar um arquivo de imagem
			Image.render()

	"""
	def __init__(self, path_export="", format_export="svg"):
		self.dot = Digraph(name='Árvore Sintática', comment='Arvore sintatica', format=format_export)

		self.path_export = path_export
		self.stack = []

	def view_graph(self):
		self.dot.view()

	# Funcao responsavel por fazer a busca de nos em meio a uma estrutura de pilhas
	def search_nodes(self,stack, node_list):
		"""
			Funcao que caminha na minha estrutura de listas dentro de listas e pega os elementos
			prioritarios para a adicao no meu grafo. Dado que a ordem de insersao importa
		"""
		if stack:
			node_list.append(stack[0])
			size_stack = len(stack)

			for i in range(1, size_stack):
				if(type(stack[i]) == list):
					self.search_nodes(stack[i], node_list)
				else:
					node_list.append(stack[i])

		return stack

	def search_edges(self,stack, node_list):
		"""
			Funcao que responsavel por caminhar sobre a minha estrutura de pilhas, e retornar
			varicas listas contendo todos os elementos que comtém ligacao.
			Obs: O primeiro elemento de toda lista, sera ligado com os demais elementos da minha
				 lista
		"""
		if stack:
			new_edges = []
			new_edges.append(stack[0])
			
			size_list = len(stack)

			for i in range(1,size_list):
				if(type(stack[i]) == list):
					new_edges.append(self.search_edges(stack[i], node_list))
				else:
					new_edges.append(stack[i])

			node_list.append(new_edges)

			return stack[0]

		return None

	def generate_nodes(self):
		"""
			Funcao responsavel por pegar os meus elementos prioritario e criar um no para 
			ele em meu grafo
		"""
		list_nodes = []
		self.search_nodes(self.stack, list_nodes)

		for i in list_nodes:
			self.dot.node(i[0], i[1])

	def generate_edges(self):
		"""
			Funcao responsavel por pegar as minhas listas de ligacoes e fazer a ligacao com
			o primeiro elemento de cada lista
		"""
		list_edges = []
		self.search_edges(self.stack, list_edges)

		for nodes in list_edges:
			for i in range(1,len(nodes)):
				self.dot.edge(nodes[0][0], nodes[i][0])
	
	def create_graph(self):
		"""
			Funcao responsavel por fazer a criacao dos nos e arestas do meu grafo
		"""
		self.generate_nodes()
		self.generate_edges()


	def translate(self, text):

		Creator = CreatorStack(text)
		self.stack = Creator.get_stack()

		self.create_graph()
	

	def g_stack(self):
		return self.stack

	def export(self, view=False):
		self.dot.render(self.path_export, view=view)



if __name__ == '__main__':
	
	
	# text = "(program (function void hugefunction () (block () () () (functions (function void example1 () (block () () (variables ((identifier_list m n s), (type integer))) () (body (statement () (function_call_statement (function_call read (expression_list (expression (simple_expression + (term (factor (variable m ())) ()) ())) (expression (simple_expression + (term (factor (variable n ())) ()) ())))))) (statement () (assignment (variable s ()) (expression (simple_expression + (term (factor (integer 0)) ()) ())))) (statement () (repetitive (expression (le (simple_expression + (term (factor (variable m ())) ()) ()) (simple_expression + (term (factor (variable n ())) ()) ()))) )))))) (function void example4 () (block () () (variables ((identifier_list m), (type integer))) (functions (function integer f (formal_parameters (expression_parameter False (identifier_list n) integer) (expression_parameter True (identifier_list k) integer)) (block () () (variables ((identifier_list p q t), (type integer))) () (body (statement () (conditional (expression (lt (simple_expression + (term (factor (variable n ())) ()) ()) (simple_expression + (term (factor (integer 2)) ()) ()))) (compound (assignment (variable k ()) (expression (simple_expression + (term (factor (integer 0)) ()) ()))) (return (expression (simple_expression + (term (factor (variable n ())) ()) ())))) (conditional_else (compound (assignment (variable t ()) (expression (simple_expression + (term (factor (function_call f (expression_list (expression (simple_expression + (term (factor (variable n ())) ()) (simple_expression_l0 - (term (factor (integer 1)) ()) ()))) (expression (simple_expression + (term (factor (variable p ())) ()) ()))))) ()) (simple_expression_l0 + (term (factor (function_call f (expression_list (expression (simple_expression + (term (factor (variable n ())) ()) (simple_expression_l0 - (term (factor (integer 2)) ()) ()))) (expression (simple_expression + (term (factor (variable q ())) ()) ()))))) ()) ())))) (assignment (variable k ()) (expression (simple_expression + (term (factor (variable p ())) ()) (simple_expression_l0 + (term (factor (variable q ())) ()) (simple_expression_l0 + (term (factor (integer 1)) ()) ()))))) (return (expression (simple_expression + (term (factor (variable t ())) ()) ()))))))))))) (body (statement () (function_call_statement (function_call write (expression_list (expression (simple_expression + (term (factor (function_call f (expression_list (expression (simple_expression + (term (factor (integer 3)) ()) ())) (expression (simple_expression + (term (factor (variable m ())) ()) ()))))) ()) ())) (expression (simple_expression + (term (factor (variable m ())) ()) ())))))))))) (body (statement () (function_call_statement (function_call example1 ()))) (statement () (function_call_statement (function_call example4 ())))))))"
	# text = "(program (function void main () (block () () (variables ((identifier_list m n s), (type integer))) () (body (statement () (assignment (variable s ()) (expression (simple_expression + (term (factor (integer 0)) ()) ())))) (statement () (assignment (variable m ()) (expression (simple_expression + (term (factor (function_call read ())) ()) ())))) (statement () (repetitive (expression (ne (simple_expression + (term (factor (variable m ())) ()) ()) (simple_expression + (term (factor (integer 0)) ()) ()))) ))) (statement () (function_call_statement (function_call write (expression_list (expression (simple_expression + (term (factor (variable s ())) ()) ()))))))))))"
	# text = "('+', ('*', ('NUM', 10), ('NUM', 50)), ('/', ('(', ('*', ('(', ('/', ('NUM', 15), ('NUM', 25)), ')'), ('NUM', 30)), ')'), ('NUM', 3)))"
	# text = "[program [function void main () [block [labels [identifier_list l0 l1]] [types [typealias integral [type integer]] [typealias bool [type boolean]]] [variables [identifier_list m n s] [type integer] [identifier_list v t] [type integer [integer 2] [integer 3] [integer 4]]] () [body [statement () [assignment [variable m ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [assignment [variable n ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [assignment [variable s ()] [expression [simple_expression + [term [factor [variable m ()]] ()] [simple_expression_l0 + [term [factor [variable n ()]] ()] ()]]]]] [statement () [function_call_statement [function_call write [expression_list [expression [simple_expression + [term [factor [variable s ()]] ()] ()]]]]]] [statement () [return ()]]]]]]"
	text = "[program [function void hugefunction () [block () () () [functions [function void example1 () [block () () [variables [var [identifier_list m n s] [type integer]]] () [body [statement () [function_call_statement [function_call read [expression_list [expression [simple_expression + [term [factor [variable m ()]] ()] ()]] [expression [simple_expression + [term [factor [variable n ()]] ()] ()]]]]]] [statement () [assignment [variable s ()] [expression [simple_expression + [term [factor [integer 0]] ()] ()]]]] [statement () [repetitive [expression [le [simple_expression + [term [factor [variable m ()]] ()] ()] [simple_expression + [term [factor [variable n ()]] ()] ()]]] )]]]]] [function void example4 () [block () () [variables [var [identifier_list m] [type integer]]] [functions [function integer f [formal_parameters [expression_parameter False [identifier_list n] integer] [expression_parameter True [identifier_list k] integer]] [block () () [variables [var [identifier_list p q t] [type integer]]] () [body [statement () [conditional [expression [lt [simple_expression + [term [factor [variable n ()]] ()] ()] [simple_expression + [term [factor [integer 2]] ()] ()]]] [compound [assignment [variable k ()] [expression [simple_expression + [term [factor [integer 0]] ()] ()]]] [return [expression [simple_expression + [term [factor [variable n ()]] ()] ()]]]] [conditional_else [compound [assignment [variable t ()] [expression [simple_expression + [term [factor [function_call f [expression_list [expression [simple_expression + [term [factor [variable n ()]] ()] [simple_expression_l0 - [term [factor [integer 1]] ()] ()]]] [expression [simple_expression + [term [factor [variable p ()]] ()] ()]]]]] ()] [simple_expression_l0 + [term [factor [function_call f [expression_list [expression [simple_expression + [term [factor [variable n ()]] ()] [simple_expression_l0 - [term [factor [integer 2]] ()] ()]]] [expression [simple_expression + [term [factor [variable q ()]] ()] ()]]]]] ()] ()]]]] [assignment [variable k ()] [expression [simple_expression + [term [factor [variable p ()]] ()] [simple_expression_l0 + [term [factor [variable q ()]] ()] [simple_expression_l0 + [term [factor [integer 1]] ()] ()]]]]] [return [expression [simple_expression + [term [factor [variable t ()]] ()] ()]]]]]]]]]]] [body [statement () [function_call_statement [function_call write [expression_list [expression [simple_expression + [term [factor [function_call f [expression_list [expression [simple_expression + [term [factor [integer 3]] ()] ()]] [expression [simple_expression + [term [factor [variable m ()]] ()] ()]]]]] ()] ()]] [expression [simple_expression + [term [factor [variable m ()]] ()] ()]]]]]]]]]] [body [statement () [function_call_statement [function_call example1 ()]]] [statement () [function_call_statement [function_call example4 ()]]]]]]]"
	# text = "[program [function void main () [block [labels [identifier_list l0 l1]] [types [typealias integral [type integer]] [typealias bool [type boolean]]] [variables [var [identifier_list m n s] [type integer]] [var [identifier_list v t] [type integer [integer 2] [integer 3] [integer 4]]]] () [body [statement () [assignment [variable m ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [assignment [variable n ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [assignment [variable s ()] [expression [simple_expression + [term [factor [variable m ()]] ()] [simple_expression_l0 + [term [factor [variable n ()]] ()] ()]]]]] [statement () [function_call_statement [function_call write [expression_list [expression [simple_expression + [term [factor [variable s ()]] ()] ()]]]]]] [statement () [return ()]]]]]]"


	Image = TreeImage('image')
	Image.translate(text)
	Image.view_graph()
	# Image.export(view=True)
	