#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Symbol Table helper"""

from .errors import SemanticError
from itertools import chain
from typing import Iterable, Optional, Tuple

# Context separator for symbols
SEP = '/'

# Descriptors
class TypeDescriptor:
    "Describes a language type"
    def __init__(self, size, name=None):
        self.size = size
        self.name = name

    def __eq__(self, value):
        # TODO Implement this in order to compare types
        if isinstance(value, Type):
            return value == self
        return self.size == value.size and self.name == value.name
    
    def __str__(self):
        return self.name if self.name is not None \
            else "<anonymous type of size %d>" % self.size

class Array(TypeDescriptor):
    "Describes an array type"
    def __init__(self, dimension_size, element_type):
        super().__init__(dimension_size * element_type.size)
        self.element_type = element_type
        self.size = dimension_size
    
    def __eq__(self, value):
        return self.size == value.size \
            and self.element_type == value.element_type
    
    def __str__(self):
        dimension = "[.]"*self.size
        return 'Array %s%s' % (self.element_type, dimension)

# It is not a type descriptor because it does not live without a function
class ParamDescriptor:
    "Describes a function parameter"
    def __init__(self, typedesc, by_reference=False):
        self.typedesc = typedesc
        self.by_reference = by_reference
    
    def __eq__(self, value):
        return self.by_reference == value.by_reference \
            and self.typedesc == value.typedesc
    
    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(self.typedesc) if not self.by_reference \
            else "var %s" % self.typedesc

class FunctionType(TypeDescriptor):
    "Describes a function with result and parameter types"

    def __init__(self,
            result: TypeDescriptor,
            parameters: Tuple[ParamDescriptor]):
        super().__init__(size=1)
        self.result = result
        self.parameters = parameters
    
    def __eq__(self, value):
        return self.result == value.result \
            and self.parameters == value.parameters
    
    def __str__(self):
        return "{}(result={},parameters={})".format(
            self.__class__.__name__,
            self.result,
            self.parameters)

class Symbol:
    def __init__(self, name: str):
        self.name = name

    def __repr__(self):
        return "{}('{}')".format(
            self.__class__.__name__,
            self.name)

    def __str__(self):
        return repr(self)

class Constant(Symbol):
    def __init__(self, name, typedesc: TypeDescriptor, value=None):
        super().__init__(name)
        self.value = value
        self.typedesc = typedesc
    
    def __repr__(self):
        return "{}('{}',value={})".format(
            self.__class__.__name__,
            self.name,
            self.value)

class Variable(Symbol):
    def __init__(self, name, displ=None, typedesc: TypeDescriptor=None, reference=False):
        super().__init__(name)
        self.displ = displ
        self.typedesc = typedesc
        self.reference = reference
    
    def __repr__(self):
        return "{}(name='{}', reference={}, type={})".format(
            self.__class__.__name__,
            self.name,
            self.reference,
            self.typedesc)

class Parameter(Symbol):
    "Symbol that references a parameter from a function"
    # This is the formal parameter
    def __init__(self, name, descriptor: ParamDescriptor):
        super().__init__(name)
        self.descriptor = descriptor

    def __repr__(self):
        return "{}('{}', type={})".format(
            self.__class__.__name__,
            self.name,
            self.descriptor)


class Function(Symbol):
    def __init__(self, name, displ=None, type: FunctionType = None):
        super().__init__(name)
        self.displ = displ
        self.type = type
    
    def __repr__(self):
        return "{}('{}',type={})".format(
            self.__class__.__name__,
            self.name,
            self.type)

class Label(Symbol):
    def __init__(self, name, address=None):
        super().__init__(name)
        self.address = address
        # self.defined = defined? # If so, why not use STRONG and WEAK for all symbols?

class Type(Symbol):
    def __init__(self, name, descriptor: TypeDescriptor):
        super().__init__(name)
        self.descriptor = descriptor

    def get_descriptor(self):
        desc = self.descriptor

        while(not isinstance(desc, TypeDescriptor) and not isinstance(desc, Array)):
            desc = desc.descriptor

        return desc

    def is_array(self, ret_array = False):
        desc = self.descriptor

        while(not isinstance(desc, TypeDescriptor) and not isinstance(desc, Array)):
            desc = desc.descriptor

        if ret_array:
            return desc

        return isinstance(desc, Array)

    def get_array(self):
        array = self.is_array(True)

        if array:
            return array
        else:
            raise SemanticError(-1, "'%s' is not array" % self.name)


    def __eq__(self, symbol):
        if isinstance(symbol, TypeDescriptor):
            return self.descriptor == symbol

        elif isinstance(symbol, Array):
            print("entrei aqui")
            return False

        elif(isinstance(symbol, Type)):
            desc = symbol.descriptor

            while(not isinstance(desc, TypeDescriptor)):
                desc = desc.descriptor

            return self.descriptor == desc
        
        return False

    def __repr__(self):
        return "{}(name='{}', descriptor={})".format(
            self.__class__.__name__,
            self.name,
            self.descriptor)

# Builtin symbols and types

class Types:
    "Builtin language type descriptors"
    BOOLEAN = TypeDescriptor(1,name='boolean')
    INTEGER = TypeDescriptor(1,name='integer')
    VOID    = TypeDescriptor(1,name='void')

    @classmethod
    def all(cls):
        return (cls.BOOLEAN,cls.INTEGER,cls.VOID)

class Symbols:
    "Builtin language symbols and constants"
    BOOLEAN = Type("boolean", Types.BOOLEAN)
    INTEGER = Type("integer", Types.INTEGER)
    VOID    = Type("void", Types.VOID)
    FALSE = Constant("false", Types.BOOLEAN, value=False)
    TRUE  = Constant("true", Types.BOOLEAN, value=True)
    READ = Function("read", type=FunctionType(
        result=Types.INTEGER,
        parameters=()
    ))
    WRITE = Function("write", type=FunctionType(
        result=None,
        parameters=(ParamDescriptor(Types.INTEGER),)
    ))

    @classmethod
    def all(cls):
        return (cls.BOOLEAN,cls.INTEGER,cls.VOID,cls.FALSE,cls.TRUE,cls.READ,cls.WRITE,)

class SymbolTable:
    """
    A table of symbols! (Its name says everything)
    This table maps names to symbols, but not limited to them. Any symbol may
    contain a context -- or scope if you want -- that is another symbol table
    with local identifiers relative to their symbol.
    So, basically a dictionary of type
        F: str -> (Symbol, SymbolTable)
    or
        F: str -> Tuple[Symbol, Optional[SymbolTable]]
    """
    def __init__(self, symbols: Iterable=(), parent=None):
        self.parent = parent
        self._table = dict(map(lambda s: (s.name, (s, None)), symbols))
    
    def add(self, symbol: Symbol):
        '''
        Adds a symbol to this table. It does not affect parent tables.
        If a symbol with the same name exists locally in this table, it raises
        a KeyError.
        '''
        if symbol.name in self._table:
            raise SemanticError(-1,"Symbol '%s' is already defined" % symbol.name)
        self._table[symbol.name] = (symbol, None)
    
    def add_with_context(self, symbol: Symbol):
        context = SymbolTable(parent=self)
        self._table[symbol.name] = (symbol, context)
        return context

    def get_table(self):
        return self._table

    def get(self, key: str, default=None):
        '''
        Returns the symbol associated with the name `key`.
        If the symbol is not present in the symbol table, this method will
        try to lookup into its parent symbol table `self.parent`.

        If the symbol is present neither in `self` nor in `self.parent`,
        the default value (None) will be returned.
        '''
        item = self._table.get(key, default)
        if item == default and self.parent:
            item = self.parent.get(key, default)
        return item
    
    def __getitem__(self, key: str) -> Symbol:
        '''
        x.__getitem__(y) <==> x[y]

        Returns the symbol associated with the name `y`.
        If the symbol is not present in the symbol table `x`, this method will
        try to lookup into its parent symbol table `self.parent`.

        If the symbol is present neither in `self` nor in `self.parent`,
        a KeyError will be raised.
        '''
        item = self.get(key, default=None)
        if item == None:
            raise SemanticError(-1, "Symbol '%s' is not defined" % key)
        return item
    
    def __contains__(self, key: str) -> bool:
        present = key in self._table
        if not present and self.parent:
            return key in self.parent
        return present
    
    def __len__(self):
        return len(self._table)
    
    def __str__(self):
        items = self._table.items()
        return "+"*70 +'\n'+ "\n".join(
            chain(
                ("{:16}{}".format(k,v[0]) for k,v in items),
                ("\nLocals of {}:\n{}".format(s,str(t))
                    for k,(s,t) in items if t is not None and len(t) > 0),
            )
        )

SYMBOL_TABLE = SymbolTable(Symbols.all())
