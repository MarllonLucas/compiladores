#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Miscelaneous classes and helpers"""

from itertools import chain

class AST:
    """
    Node representation for an Abstract Syntax Tree
    """

    def __init__(self, name: str, *args, pos=()):
        self.name = name
        self.args = args
        self.pos = pos
    
    def __repr__(self):
        return "[{}]".format(' '.join(chain((self.name,), map(str, self.args))))
    
    def __str__(self):
        return repr(self)
