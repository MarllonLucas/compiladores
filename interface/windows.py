#!/usr/bin/env python3

import gi
import os
gi.require_version("Gtk", "3.0")
gi.require_version('Vte', '2.91')
gi.require_version("GtkSource", "3.0")

from gi.repository import Gtk, Vte, GLib, Pango, Gio, Gdk, GtkSource

class Interpleter(Gtk.Window):
    def __init__(self, file_mepa):
        Gtk.Window.__init__(self, title="Interpletador MEPA")
        self.set_default_size(980, 720)

        self.layout = None
        self.buffer_mepa = None
        self.file_mepa = file_mepa

        self.pty = Vte.Pty.new_sync(Vte.PtyFlags.DEFAULT)
        self.terminal = Vte.Terminal()
        self.terminal.set_pty(self.pty)
        self.terminal.set_input_enabled(False)
        self.terminal.connect("child-exited", self.final_process_terminal) 
        
        self.terminal.set_color_background(self.parse_rgba("#FFF"))
        self.terminal.set_color_foreground(self.parse_rgba("#232323"))
        self.terminal.set_color_bold(self.parse_rgba("#000"))
        self.terminal.set_color_cursor(self.parse_rgba("#232323"))
        self.terminal.set_color_highlight(self.parse_rgba("#999"))

        self.create_layout()
        self.load_file()

        self.add(self.layout)
        self.show_all()

    def load_file(self):
        with open(self.file_mepa) as file:
            content = file.read()

        self.buffer_mepa.set_text(content)

    def save_file(self):
        if self.file_mepa:
            code = self.read_buffer(self.buffer_mepa)
            
            with open(self.file_mepa, 'w+') as file:
                file.writelines(code)

    def read_buffer(self, buff):
        start, end, hidden = buff.get_start_iter(), buff.get_end_iter(), True
        return buff.get_text(start, end, hidden)
        
    def parse_rgba(self, color):
        col = Gdk.RGBA()
        col.parse(color)
        return col

    def create_layout(self):
        self.layout = Gtk.Grid()
        self.layout.set_row_spacing(4)
        self.layout.set_column_homogeneous(True)

        # ------ Pager ------
        widget_pager = Gtk.Stack()
        # button change page
        widget_pager_switcher = Gtk.StackSwitcher()
        widget_pager_switcher.set_stack(widget_pager)
        widget_pager_switcher.set_orientation(Gtk.Orientation.VERTICAL)
        widget_pager_switcher.set_margin_top(10)
        widget_pager_switcher.set_margin_bottom(10)
        widget_pager_switcher.set_margin_left(10)
        widget_pager_switcher.set_margin_right(10)

        # ------ Pager Code ------
        widget_editor, self.buffer_mepa, editor = self.get_page_editor()

        # ------ Pager interpleter ------
        page_interp = Gtk.Grid()
        page_interp.set_row_spacing(4)
        page_interp.set_margin_top(30)
        page_interp.set_margin_bottom(10)
        page_interp.set_margin_right(30)
        page_interp.set_margin_left(30)
        page_interp.set_column_homogeneous(True)

        widget_action_bar = Gtk.ActionBar()
        widget_action_bar.pack_end(self.get_widget_button("Executar", self.on_execute))

        widget_terminal = Gtk.ScrolledWindow()
        widget_terminal.set_hexpand(True)
        widget_terminal.set_vexpand(True)
        widget_terminal.add(self.terminal)

        label = Gtk.Label()
        label.set_markup("<big>Interpletador MEPA</big>\n<small>Click em executar e insira os valores de entrada do programa no terminal abaixo</small\n>")
        label.set_halign(Gtk.Align.START)
        label.set_margin_bottom(10)

        page_interp.attach(label, 0, 0, 1, 1)
        page_interp.attach(widget_terminal, 0, 1, 1, 1)
        page_interp.attach(widget_action_bar, 0, 2, 1, 1)

        widget_pager.add_titled(widget_editor, 'page_code', "Codigo MEPA")
        widget_pager.add_titled(page_interp, 'page_interpleter', "Interpletador")
        # widget_pager.add_titled(page_config, 'page_config', "Preferencias")

        self.layout.attach(widget_pager_switcher, 0, 0, 1, 1)
        self.layout.attach_next_to(widget_pager, widget_pager_switcher, Gtk.PositionType.RIGHT, 3, 1)

    def get_page_editor(self):
        widget_scrolled_window = Gtk.ScrolledWindow()
        widget_scrolled_window.set_hexpand(True)
        widget_scrolled_window.set_vexpand(True)

        editor  = GtkSource.View()
        buff    = editor.get_buffer()

        lm = GtkSource.LanguageManager()
        sm = GtkSource.StyleSchemeManager()
    
        buff.set_language(lm.get_language('plain'))
        buff.set_highlight_syntax(True)
        
        editor.set_show_line_numbers(True)
        editor.set_auto_indent(True)
        editor.set_sensitive(True)
        editor.set_highlight_current_line(True)
        editor.set_monospace(True)
        editor.set_pixels_above_lines(2)
        editor.set_pixels_inside_wrap(2)
        editor.set_right_margin(2)
        editor.set_show_line_marks(True)
        editor.set_smart_backspace(True)
        editor.set_wrap_mode(Gtk.WrapMode(2))

        widget_scrolled_window.add(editor)

        return widget_scrolled_window, buff, editor

    def get_widget_separator(self, orientation):
        return Gtk.Separator.new(Gtk.Orientation(orientation))

    def get_widget_switch(self, activate = True, signal = None):
        switch = Gtk.Switch()
        switch.set_state(activate)
        switch.set_halign(Gtk.Align.END)

        if signal:
            switch.connect("notify::active", signal)

        return switch

    def get_widget_label(self, title, subtitle):
        text = "<b>%s</b>\n<small>%s</small>" % (title, subtitle)
        label = Gtk.Label.new()
        label.set_markup(text)
        label.set_halign(Gtk.Align.START) 
        return label

    def get_widget_button(self, label, signal, image = None):
        btn = Gtk.Button()
        btn.connect("clicked", signal)
        btn.set_label(label)
        btn.set_visible(True)
        btn.set_focus_on_click(True)

        return btn

    def final_process_terminal(self, *args):
        self.terminal.set_input_enabled(False)

    def get_arguments(self):
        arguments = ["/usr/bin/python3", "libs/interpleter/mepa.py", '--progfile', self.file_mepa, '--debug']
        return arguments

    def on_execute(self, *args):
        
        self.save_file()

        flags = Vte.PtyFlags.DEFAULT
        c_dir = os.getcwd() 
        args = self.get_arguments()
        spawn = GLib.SpawnFlags.DO_NOT_REAP_CHILD
        self.terminal.spawn_sync(flags,c_dir,args,[],spawn,None,None,)
        self.terminal.set_input_enabled(True)

class HelpDialog(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(
            self,
            "Ajuda",
            parent,
            0,
            (   
                Gtk.STOCK_OK,
                Gtk.ResponseType.OK
            ),
        )
        self.parent = parent
        self.set_default_size(500,300)

        self.notebook = Gtk.Notebook()

        self.notebook.append_page(self.help_execute(), Gtk.Label("Como executar"))
        self.notebook.append_page(self.help_flow(), Gtk.Label("Fluxo de execução"))
        self.notebook.append_page(self.help_map(), Gtk.Label("Mapa de Botões"))
        self.notebook.append_page(self.help_dev(), Gtk.Label("Desenvolvedores"))

        box = self.get_content_area()
        box.add(self.notebook)

        self.show_all()

    def get_buffer_markup(self, text):
        buff  = Gtk.TextBuffer()
        buff.insert_markup(buff.get_start_iter(), text, -1)

        return  buff

    def get_text_view(self, buff):
        view = Gtk.TextView()
        view.set_buffer(buff)
        view.set_editable(False)
        view.set_cursor_visible(False)
        view.set_top_margin(10)
        view.set_left_margin(10)
        view.set_right_margin(10)
        view.set_bottom_margin(10)
        view.set_name("help_div")

        return view

    def get_box_page(self, text):
        buff = self.get_buffer_markup(text)
        view = self.get_text_view(buff)

        widget_box = Gtk.Box()
        widget_box.set_border_width(10)
        widget_box.add(view)

        return widget_box

    def help_execute(self):
        text  = "<b>1)</b> Primeiro selecione um arquivo no botão do canto superior esquerdo.\n"
        text += "<b>2)</b> Depois de selecionar, execute a ação desejada nos botões de ações\n"
        text += "que estão na parte inferior da interface.\n\n"
        text += "\t<i>Caso queria gerar uma imagem de um grafo representando a árvore\n"
        text += "\tsintática quando executar a ação de análise sintática,\n"
        text += "\tdeixe a opção de <span foreground=\"#FF0000\">MOSTRAR GRAFO</span> ativada.</i>\n\n"

        return self.get_box_page(text)

    def help_flow(self):
        text  = "Depois de carregar o arquivo selecionado, o sistema, primeiro carregaum\n"
        text += "preview do seu código na aba <span foreground=\"#FF0000\">Codigo</span>, depois que é realizado uma ação de analise\n"
        text += "(a escolha do usuário), a saida do analizador é mostrado na aba <span foreground=\"#FF0000\">Saida</span>.\n\n"
        text += "Na aba <span foreground=\"#FF0000\">Saida</span> é mostrado todas as execuções realizadas, elas ficam dispostas\n"
        text += "de forma que a ultima execução do analizador fica no inicio da tela.\n\n"
        text += "\t<i>É possivel fazer a realizar a ação de limpeza desta aba\n"
        text += "\tclicando no botão <span foreground=\"#FF0000\">Limpar Saida</span>, que fica no canto inferior direito.</i>\n\n"

        return self.get_box_page(text)

    def help_map(self):
        text  = "- <b>Seletor de arquivo</b>\n"
        text += "\tResponsável por fazer a seleção do programa para a compilação\n\n"
        text += "- <b>Mostrar grafo</b>\n"
        text += "\tResponsável por sinalizar para o analizador sintatico gerar uma imagem\n"
        text += "\tde representação da árvore sintatica\n\n"
        text += "- <b>Habilitar edição</b>\n"
        text += "\tResponsável por habilitar ou desabilitar o editor de códigos\n"
        text += "\t\tCaso não tenha nenhum arquivo selecionado, por definição, ele é setado como desabilitado\n\n"
        text += "- <b>Codigo</b>\n"
        text += "\tFaz a troca para a página para o editor do codigo\n\n"
        text += "- <b>Saida</b>\n"
        text += "\tFaz a troca para a página de resultado dos analizadores\n\n"
        text += "- <b>Lexico</b>\n"
        text += "\tResponsável por fazer a execução da analise léxica do arquivo selecionado\n\n"
        text += "- <b>Sintatico</b>\n"
        text += "\tResponsável por fazer a execução da analise sintatica do arquivo selecionado\n"
        text += "\t\tCaso o sinalizador de <span foreground=\"#FF0000\">Mostrar grafo</span> estiver ativo, ao executar essa função\n"
        text += "\t\to sistema irá gerar uma representação gráfica da árvore sintática, gerada\n"
        text += "\t\tpelo analizador\n\n"
        text += "- <b>Semantico</b>\n"
        text += "\tResponsável por fazer a execução da analise semantica do arquivo selecionado\n"
        text += "\t\t<i>Sua saida é a tabela de simbolos gerada pelo analizador</i>\n"
        text += "\t\t<i>Caso haja erros semanticos, será exibido uma mensagem de alerta na interface.</i>\n\n"
        text += "- <b>Gerador de codigo</b>\n"
        text += "\tResponsável por fazer a geração do código MEPA\n"
        text += "\t\t<i>Ira abrir uma nova janela ao qual é possivel fazer uma interpletação do código gerado</i>\n\n"
        
        text += "- <b>Limpar saida</b>\n"
        text += "\tResponsável por fazer a limpeza a página de <span foreground=\"#FF0000\">Saida</span>\n"

        return self.get_box_page(text)

    def help_dev(self):
        text = "<big><big><b>Desenvolvidor por:</b></big></big>\n\n"
        text += "\t- <span foreground=\"#FF0000\"><b>Marllon Rodrigues</b></span>\n"
        text += "\t- <span foreground=\"#009900\"><b>Felipe Machado</b></span>\n"
        text += "\t- <span foreground=\"#0000FF\"><b>Rodrigo Schio</b></span>\n\n\n\n"
        text += "<small><b><i>Interface desenvolvida inteiramente em GTK3</i></b></small>\n\n"
        text += "<big>Interpletador MEPA</big>\n"
        text += """
Copyright (c) 2015
\t\tTomasz Kowaltowski
\t\tInstitute of Computing
\t\tUniversity of Campinas
\t\tCampinas, SP, Brazil
\twww.ic.unicamp.br/~tomasz"""

        return self.get_box_page(text)