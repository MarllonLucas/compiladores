# Trabalho de compiladores

## Requisitos mínimos

Python 3.5 ou mais recente.

Pacotes:

- [ply](https://pypi.org/project/ply/).
- [gtk3](https://python-gtk-3-tutorial.readthedocs.io/en/latest/install.html)
- [graphviz](https://pypi.org/project/graphviz/)

## Organização do projeto

Há quatro *scripts* executáveis principais, um para cada estágio de análise do compilador:

- [interface.py](interface.py);
- [lexical.py](lexical.py);
- [semantic.py](semantic.py);
- [syntax.py](syntax.py).

*Scripts* para o estágio de síntese não foram desenvolvidos.

Além destes, há alguns módulos auxiliares:

- [errors.py](libs/errors.py), para definições de classes de erros;
- [misc.py](libs/misc.py), que contém uma classe genérica para representar a *Abstract Syntax Tree* (AST);
- [symboltable.py](libs/symboltable.py), que possui definições de tipo e demais símbolos da linguagem, além de uma tabela de símbolos inicial.
- [graph_generator.py](libs/graph_generator.py), faz a criação da representação da árvore sinteatica em uma imagem
- [creator_stack.py](libs/creator_stack.py), classe responsavel por pegar uma representação da árvore sintatica no formato de texto, e transformala em uma estrutura de pilhas para a geração da imagem utilizando o script graph_generator.py.

No diretório [docs/](docs/) existem arquivos sobre a especificação da linguagem e em [tests/](tests/) há códigos de teste a serem compilados.

O diretório [libs/](libs/) é responsável por armazenar as classes auxiliares que o sistema utiliza

### Symbol Table

A tabela de símbolos é um dicionário que mapeia nomes (identificadores ou *strings*) para seu respectivo símbolo e para uma tabela de símbolos local relativo ao nome quando necessário. Mais especificamente, a tabela é um dicionário *S* tal que:

- `S: str -> (Symbol, SymbolTable)`, ou
- `S: str -> Tuple[Symbol, Optional[SymbolTable]]`.

Uma função *f* qualquer, por exemplo, contém seus próprios identificadores e variáveis visíveis apenas localmente. Por isso, a entrada de *f* na tabela *S* possuirá uma tabela própria apenas para este identificadores locais.

# Execução

Em sistemas Unix, assegure que os *scripts* abaixo tenham permissão de execução:
```bash
$ chmod +x lexical.py semantic.py syntax.py interface.py
```

## Interface

Para a execução da interface, basta ter as libs necessárias instaladas para a execução da mesma,
para executa-la basta fazer:

```bash
$ ./interface.py
```

Na interface você tem acesso a todas as funcionalidades implementadas neste trabalho.

## Analisador léxico

Ele apenas requer como entrada um código-fonte da entrada padrão (*stdin*). Execute com o operador `<` de redirecionamento da shell ou copie e cole a entrada no console:

```bash
$ ./lexical.py < tests/program1.txt
```

A saída esperada é uma sequência de tokens:

```
  1 VOID             void
  1 IDENTIFIER       HugeFunction
  1 LPAREN           (
  1 RPAREN           )
  2 FUNCTIONS        functions
  3 VOID             void
  3 IDENTIFIER       Example1
  3 LPAREN           (
  3 RPAREN           )
  4 VARS             vars
[...]
```

## Analisador sintático

Sua execução é similar ao analisador léxico e recebe o código fonte da entrada padrão (*stdin*). Execute com:

```bash
$ ./syntax.py < tests/sum_loop.txt
```

A saída esperada (*stdout*) é uma sequência uma AST no seguinte formato:

```
[program [function void main () [block () () [variables [var [identifier_list m n s] [type integer]]] () [body [statement () [assignment [variable s ()] [expression [simple_expression + [term [factor [integer 0]] ()] ()]]]] [statement () [assignment [variable m ()] [expression [simple_expression + [term [factor [function_call read ()]] ()] ()]]]] [statement () [repetitive [expression [ne [simple_expression + [term [factor [variable m ()]] ()] ()] [simple_expression + [term [factor [integer 0]] ()] ()]]] )]] [statement () [function_call_statement [function_call write [expression_list [expression [simple_expression + [term [factor [variable s ()]] ()] ()]]]]]]]]]]
```

Cada nó da árvore é representada por `[name args...]` em *name* é o nome do nó e *args...* é uma sequência de itens filhos que podem ser valores ou outros nós. É uma representação inspirada pela AST da linguagem [Julia](https://docs.julialang.org/en/v1/devdocs/ast/index.html).

## Analisador semântico

Sua execução é similar à analisadores anteriores e recebe o código fonte da entrada padrão (*stdin*). Execute com:

```bash
$ ./semantic.py < tests/program1.txt
```

O analizição percorre pelo programa armazenando na tabela de simbolos todas as declarações
de variaveis, tipos, labels e funções. O mesmo é responsavel por fazer verificações de operações 
validas, e invalidas. Caso não haja erros de semantica, o analizador irá mostrar a tabela de simbolos completa juntamente com o seu contexto.

```
boolean         Type('boolean',descriptor=boolean)
integer         Type('integer',descriptor=integer)
false           Constant('false',value=False)
true            Constant('true',value=True)
read            Function('read',type=FunctionType(result=integer,parameters=()))
write           Function('write',type=FunctionType(result=None,parameters=(integer,)))
HugeFunction    Function('HugeFunction',type=None)
[...]
```

## Alunos

Felipe Machado (2017.1904.014-1)  
Marllon Rodrigues (2017.1904.045-1)  
Rodrigo Schio (2017.1904.001-0)
