#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from libs.errors import CompilerError
from libs.misc import AST

from lexical import tokens, build as build_lexer
from ply import yacc

import os, sys

DEBUG = os.environ.get('DEBUG', '0').lower() in ('true', '1')

# O YACC aceita apenas produções na Forma Normal de Backus-Naur.
# Grammar BNF

def p_program(p):
    '''
    program : function
    '''
    p[0] = AST('program', p[1], pos=(p.lineno(0),p.lexspan(0)))

def p_function(p):
    '''
    function : function_return identifier formal_parameters block
    '''
    p[0] = AST('function', *p[1:], pos=(p.lineno(0),p.lexspan(0)))

def p_function_return(p):
    '''
    function_return : identifier
        | VOID
    '''
    p[0] = p[1]

def p_block(p):
    '''
    block : labels_e types_e variables_e functions_e body
    '''
    p[0] = AST('block', *p[1:], pos=(p.lineno(0),p.lexspan(0)))

def p_labels_e(p):
    '''
    labels_e : labels
        | empty
    '''
    p[0] = p[1]

def p_types_e(p):
    '''
    types_e : types
        | empty
    '''
    p[0] = p[1]

def p_variables_e(p):
    '''
    variables_e : variables
        | empty
    '''
    p[0] = p[1]

def p_functions_e(p):
    '''
    functions_e : functions
        | empty
    '''
    p[0] = p[1]

def p_labels(p):
    '''
    labels : LABELS identifier_list SEMICOL
    '''
    p[0] = AST('labels', p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_types(p):
    '''
    types : TYPES types_l1
    '''
    p[0] = AST('types', *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_types_l1(p):
    '''
    types_l1 : identifier EQ type SEMICOL types_l0
    '''
    t = AST('typealias', p[1], p[3])
    p[0] = (t,) + p[5]

def p_types_l0(p):
    '''
    types_l0 : identifier EQ type SEMICOL types_l0
        | empty
    '''
    if p[1]:
        t = AST('typealias', p[1], p[3])
        p[0] = (t,) + p[5]
    else:
        p[0] = ()

def p_variables(p):
    '''
    variables : VARS variables_l1
    '''
    p[0] = AST('variables', *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_variables_l1(p):
    '''
    variables_l1 : identifier_list COL type SEMICOL variables_l0
    '''
    e = AST('var', p[1], p[3])
    p[0] = (e,) + p[5]

def p_variables_l0(p):
    '''
    variables_l0 : identifier_list COL type SEMICOL variables_l0
        | empty
    '''
    if p[1]:
        e = AST('var', p[1], p[3])
        p[0] = (e,) + p[5]
    else:
        p[0] = ()

def p_functions(p):
    '''
    functions : FUNCTIONS functions_l1
    '''
    p[0] = AST('functions', *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_functions_l1(p):
    '''
    functions_l1 : function functions_l0
    '''
    p[0] = (p[1],) + p[2]

def p_functions_l0(p):
    '''
    functions_l0 : function functions_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[1],) + p[2]
    else:
        p[0] = ()

def p_body(p):
    '''
    body : LBRACE body_l0 RBRACE
    '''
    p[0] = AST('body', *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_body_l0(p):
    '''
    body_l0 : statement body_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[1],) + p[2]
    else:
        p[0] = ()

def p_type(p):
    '''
    type : identifier type_l0
    '''
    p[0] = AST('type', p[1], pos=(p.lineno(0),p.lexspan(0))) if not p[2] else AST('type', p[1], *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_type_l0(p):
    '''
    type_l0 : LBRACK integer RBRACK type_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[2],) + p[4]
    else:
        p[0] = ()

def p_formal_parameters(p):
    '''
    formal_parameters : LPAREN formal_parameters_i RPAREN
    '''
    # (formal_parameters [expression_parameter|function_parameter]...) | ()
    if p[2]:
        p[0] = AST('formal_parameters', *p[2], pos=(p.lineno(0),p.lexspan(0)))
    else:
        p[0] = ()

def p_formal_parameters_i(p):
    '''
    formal_parameters_i : formal_parameter formal_parameters_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[1],) + p[2]
    else:
        p[0] = ()

def p_formal_parameters_l0(p):
    '''
    formal_parameters_l0 : SEMICOL formal_parameter formal_parameters_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[2],) + p[3]
    else:
        p[0] = ()

def p_formal_parameter(p):
    '''
    formal_parameter : expression_parameter
        | function_parameter
    '''
    p[0] = p[1]

def p_expression_parameter(p):
    '''
    expression_parameter : VAR identifier_list COL identifier
        | identifier_list COL identifier
    ''' # it should be "identifier_list COL type"
    # (expression_parameter by_reference identifier_list type)
    i = int(isinstance(p[1], str))
    p[0] = AST('expression_parameter', bool(i), p[i+1], p[i+3], pos=(p.lineno(0),p.lexspan(0)))

def p_function_parameter(p):
    '''
    function_parameter : function_return identifier formal_parameters
    '''
    p[0] = AST('function_parameter', *p[1:], pos=(p.lineno(0),p.lexspan(0)))

def p_statement(p):
    '''
    statement : identifier COL unlabeled_statement
        | unlabeled_statement
        | compound
    '''
    # (statement label [unlabeled_statement|compound])
    if len(p) == 4:
        p[0] = AST('statement', p[1], p[3], pos=(p.lineno(0),p.lexspan(0)))
    else:
        p[0] = AST('statement', (), p[1], pos=(p.lineno(0),p.lexspan(0)))

def p_variable(p):
    '''
    variable : identifier variable_l0
    '''
    p[0] = AST('variable', *p[1:], pos=(p.lineno(0),p.lexspan(0)))

def p_variable_l0(p):
    '''
    variable_l0 : LBRACK expression RBRACK variable_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[2],) + p[4]
    else:
        p[0] = ()

def p_unlabeled_statement(p):
    '''
    unlabeled_statement : assignment
        | function_call_statement
        | goto
        | return
        | conditional
        | repetitive
        | empty_statement
    '''
    p[0] = p[1]

def p_assignment(p):
    '''
    assignment : variable EQ expression SEMICOL
    '''
    p[0] = AST('assignment', p[1], p[3], pos=(p.lineno(0),p.lexspan(0)))

def p_function_call_statement(p):
    '''
    function_call_statement : function_call SEMICOL
    '''
    p[0] = AST('function_call_statement', p[1], pos=(p.lineno(0),p.lexspan(0)))

def p_goto(p):
    '''
    goto : GOTO identifier SEMICOL
    '''
    p[0] = AST('goto', p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_return(p):
    '''
    return : RETURN expression_e SEMICOL
    '''
    p[0] = AST('return', p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_expression_e(p):
    '''
    expression_e : expression
        | empty
    '''
    if p[1]:
        p[0] = p[1]
    else:
        p[0] = ()

def p_compound(p):
    '''
    compound : LBRACE compound_l1 RBRACE
    '''
    p[0] = AST('compound', *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_compound_l1(p):
    '''
    compound_l1 : unlabeled_statement compound_l0
    '''
    p[0] = (p[1],) + p[2]

def p_compound_l0(p):
    '''
    compound_l0 : unlabeled_statement compound_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[1],) + p[2]
    else:
        p[0] = ()

def p_conditional(p):
    '''
    conditional : IF LPAREN expression RPAREN compound conditional_else
    '''
    p[0] = AST('conditional', p[3], *p[5:], pos=(p.lineno(0),p.lexspan(0)))

def p_conditional_else(p):
    '''
    conditional_else : ELSE compound
        | empty
    '''
    if len(p) == 3:
        p[0] = AST('conditional_else', p[2], pos=(p.lineno(0),p.lexspan(0)))
    else:
        p[0] = ()

def p_repetitive(p):
    '''
    repetitive : WHILE LPAREN expression RPAREN compound
    '''
    p[0] = AST('repetitive', p[3], p[5], pos=(p.lineno(0),p.lexspan(0)))

def p_empty_statement(p):
    '''
    empty_statement : SEMICOL
    '''
    p[0] = AST('nop', 0, 0)

RELOP_DICT = {
    '==': 'eq',
    '<=': 'le',
    '>=': 'ge',
    '!=': 'ne',
    '<' : 'lt',
    '>' : 'gt',
}

def p_expression(p):
    '''
    expression : simple_expression expression_i
    '''
    pos = (p.lineno(1), p.lexspan(1),)
    if p[2]: # expression_i is not empty
        # Instead of doing
        # (expression simple_expression (relational_operator simple_expression))
        # We do
        # (expression (relational_operator simple_expression simple_expression))
        # This way we eliminate the use of L-attributes
        relop, expr = p[2]
        p[0] = AST('expression', AST(RELOP_DICT[relop], p[1], expr, pos=pos), pos=pos)
    else:
        p[0] = AST('expression', p[1], pos=pos)

def p_expression_i(p):
    '''
    expression_i : relational_operator simple_expression
        | empty
    '''
    if p[1]:
        p[0] = (p[1], p[2])
    else:
        p[0] = ()

def p_relational_operator(p):
    '''
    relational_operator : RELOP
    '''
    p[0] = p[1]

def p_simple_expression(p):
    '''
    simple_expression : PLUS term simple_expression_l0
        | MINUS term simple_expression_l0
        | term simple_expression_l0
    '''
    i = int(isinstance(p[1], str))
    unaryop = p[1] if i else '+' # defaults to + term...
    # (simple_expression unaryop term simple_expression_l0)
    p[0] = AST('simple_expression', unaryop, *p[i+1:], pos=(p.lineno(0),p.lexspan(0)))


def p_simple_expression_l0(p):
    '''
    simple_expression_l0 : additive_operator term simple_expression_l0
        | empty
    '''
    if p[1]:
        p[0] = AST('simple_expression_l0', *p[1:], pos=(p.lineno(0),p.lexspan(0)))
    else:
        p[0] = ()

def p_additive_operator(p):
    '''
    additive_operator : PLUS
        | MINUS
        | OR
    '''
    p[0] = p[1]

def p_term(p):
    '''
    term : factor term_l0
    '''
    p[0] = AST('term', *p[1:], pos=(p.lineno(0),p.lexspan(0)))

def p_term_l0(p):
    '''
    term_l0 : multiplicative_operator factor term_l0
        | empty
    '''
    if p[1]:
        p[0] = AST('term_l0', *p[1:], pos=(p.lineno(0),p.lexspan(0)))
    else:
        p[0] = ()

def p_multiplicative_operator(p):
    '''
    multiplicative_operator : TIMES
        | DIVIDE
        | EE
    '''
    p[0] = AST('multiplicative_operator', p[1], pos=(p.lineno(0),p.lexspan(0)))

def p_factor(p):
    '''
    factor : variable
        | integer
        | function_call
        | LPAREN expression RPAREN
        | NOT factor
    '''
    if p[1] == '!': # NOT
        p[0] = AST('factor', AST('not', p[2], pos=(p.lineno(1), p.lexspan(1))), pos=(p.lineno(0),p.lexspan(0)))
    elif p[1] == '(': # LPAREN
        p[0] = AST('factor', p[2], pos=(p.lineno(0),p.lexspan(0)))
    else:
        # I think we should simplify: p[0] = p[1]
        p[0] = AST('factor', p[1], pos=(p.lineno(0),p.lexspan(0)))

def p_function_call(p):
    '''
    function_call : identifier LPAREN expression_list RPAREN
    '''
    p[0] = AST('function_call', p[1], p[3], pos=(p.lineno(0),p.lexspan(0)))

def p_identifier_list(p):
    '''
    identifier_list : identifier identifier_list_l0
    '''
    # Huge simplication going here: we will eliminate identifier_list_l0 from
    # AST
    # (identifier_list identifier...)
    p[0] = AST('identifier_list', p[1], *p[2], pos=(p.lineno(0),p.lexspan(0)))

def p_identifier_list_l0(p):
    '''
    identifier_list_l0 : COMMA identifier identifier_list_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[2],) + p[3]
    else:
        p[0] = ()

def p_expression_list(p):
    '''
    expression_list : expression expression_list_l0
        | empty
    '''
    # (expression_list expression...)
    if p[1]:
        p[0] = AST('expression_list', p[1], *p[2], pos=(p.lineno(0),p.lexspan(0)))
    else:
        p[0] = ()

def p_expression_list_l0(p):
    '''
    expression_list_l0 : COMMA expression expression_list_l0
        | empty
    '''
    if p[1]:
        p[0] = (p[2],) + p[3]
    else:
        p[0] = ()

def p_empty(p):
    '''
    empty :
    '''
    p[0] = ()

def p_integer(p):
    '''
    integer : NUMBER
    '''
    p[0] = AST('integer', p[1], pos=(p.lineno(0),p.lexspan(0)))

def p_identifier(p):
    '''
    identifier : IDENTIFIER
        | INTEGER
        | BOOLEAN
        | TRUE
        | FALSE
        | READ
        | WRITE
    '''
    p[0] = p[1]

def p_error(p):
    if(p):
        raise CompilerError(p.lineno, "Unexpected token: '%s'" % p.value)
    else:
        raise CompilerError(-1, "Error EOF")

def build():
    return yacc.yacc(tabmodule="output.parsetab")

def syntax_graphic(data):
    try:
        build()
        
        ast = yacc.parse(
            data,
            lexer=build_lexer(),
            tracking=True, # it's necessary to obtain position of the token
            debug=DEBUG
            )

        return True, ast    
    
    except Exception as e:
        return False, e

def main():

    parser = build()

    data = sys.stdin.read()

    ast = parser.parse(
        data,
        lexer=build_lexer(),
        tracking=True, # it's necessary to obtain position of the token
        debug=DEBUG
    )

    print(ast)


if __name__ == "__main__":
    try:
        main()
    except CompilerError as e:
        print(e, file=sys.stderr)
        exit(1)
